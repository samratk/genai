import streamlit as st
import pandas as pd

# Title of the app
st.title('My First Streamlit App')

# Create a DataFrame
data = pd.DataFrame({
    'Column A': [2, 2, 3, 4],
    'Column B': [11, 20, 30, 40]
})

# Display the DataFrame
st.write(data)

# Add a slider
value = st.slider('Select a value', 1, 100, 50)

# Display the selected value
st.write('Selected value:', value)
